package com.example.workshopapp.controller;

import com.example.workshopapp.entities.Produkt;
import com.example.workshopapp.repository.ProduktRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

    @Autowired
    private ProduktRepo repo;

    @GetMapping("/")
    public String helloWorld()
    {
        return "Hello, World!";
    }


    @GetMapping("/get-products")
    public String getAll()
    {
        return repo.findAll().stream().map(p -> ("Id: " + p.getId() + ";<br>Name: " + p.getName() + "<br>Price: " + p.getPrice()+"<br><br>")).reduce("", (a,b) -> a+b);
    }

    @PostMapping("/create-product")
    public void createProduct(@RequestParam String name,@RequestParam int price)
    {
        Produkt p = new Produkt();
        p.setName(name);
        p.setPrice(price);
        repo.saveAndFlush(p);
    }




}
