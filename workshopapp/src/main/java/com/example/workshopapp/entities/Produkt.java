package com.example.workshopapp.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "produkt")
public class Produkt {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private int price;

    public Produkt(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Produkt() {

    }
}
