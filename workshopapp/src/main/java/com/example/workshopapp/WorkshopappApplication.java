package com.example.workshopapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkshopappApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkshopappApplication.class, args);
    }

}
