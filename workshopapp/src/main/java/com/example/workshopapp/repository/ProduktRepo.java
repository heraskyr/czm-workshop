package com.example.workshopapp.repository;

import com.example.workshopapp.entities.Produkt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProduktRepo extends JpaRepository<Produkt, Integer>
{
}
